const mainNav = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/Home/index.vue'),
    redirect: { name: 'videos' },
    meta: {
      type: 'home',
    },
  },
  {
    path: '/videos',
    name: 'videos',
    component: () => import('../views/Videos/index.vue'),
    meta: {
      type: 'main',
    },
  }
];

export { mainNav };
