from flask import Flask, jsonify
from celery import Celery
from .config import Config

app = Flask(__name__)
app.config.from_object(Config)

CELERY_TASK_LIST = [
    'main.tasks'
]


def make_celery():
    celery = Celery(
        broker=app.config['CELERY_BROKER_URL'],
        backend=app.config['CELERY_RESULT_BACKEND'],
        include=CELERY_TASK_LIST
    )

    celery.conf.task_routes = {
        'web.*': {'queue': 'web'}
    }

    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


@app.route('/main')
def async_job():
    from .tasks import upload

    local_path = 'sample.MP4'
    path = 'celery-videos/sample.MP4'

    upload.apply_async(args=[local_path, path], ignore_result=True)

    return jsonify('Start uploading...')
