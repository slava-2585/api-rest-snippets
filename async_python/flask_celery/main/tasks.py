from . import make_celery
from .s3client import upload_video

celery = make_celery()


@celery.task(name='web.upload')
def upload(local_path, path):
    upload_video(local_path, path)
    return 'success'
